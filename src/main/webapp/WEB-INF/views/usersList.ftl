<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="style.css">
</head>
<body style="background-color: #00bcd4">
<h1 style="font: 32px 'Arial Black'">Users list</h1>
<table border="1" cellpadding="10" width="65%">
    <tr>
        <th width="2%">Id</th>
        <th >Name</th>
        <th>Email</th>
        <th width="5%">Age</th>
        <th width="15%">Money</th>
    </tr>
<#list users as user>
    <tr>
        <td><a href="/TestWebJava_war/user/${user.id}">${user.id}</a></td>
        <td>${user.name}</td>
        <td>${user.email}</td>
        <td style="text-align: right">${user.age}</td>
        <td style="text-align: right">${user.money}</td>
        <td><a href="/TestWebJava_war/delete/${user.id}">Delete</a></td>
        <td><a href="/TestWebJava_war/update/${user.id}">Update</a></td>
    </tr>
</#list>
</table>

<br>
<form action="/TestWebJava_war/addUser">
    <input type="submit" value="Create User" />

</body>
</html>