<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create User Page</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<form name="user" action="/TestWebJava_war/updateUser" method="post">
    <p>ID</p>
    <input title="Id" type="text" name="Id" value="${user.id}" readonly>
    <p>Name</p>
    <input title="Name" type="text" name="name" value="${user.name}">
    <p>Email</p>
    <input title="Email" type="text" name="email" value="${user.email}">
    <p>Age</p>
    <input title="Age" type="text" name="age" value="${user.age}">
    <p>Money</p>
    <input title="Money" type="text" name="money" value="${user.money}">
    <input type="submit" value="OK">
</form>

</body>
</html>