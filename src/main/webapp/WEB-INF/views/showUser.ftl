<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User Info</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<h1>User info</h1>
<table>
    <tr>
        <td>ID</td>
        <td>${user.id}</td>
    </tr>
    <tr>
        <td>Name</td>
        <td>${user.name}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>${user.email}</td>
    </tr>
    <tr>
        <td>Age</td>
        <td>${user.age}</td>
    </tr>
    <tr>
        <td>Money</td>
        <td>${user.money}</td>
        <td><a href="/TestWebJava_war/transfer/${user.id}">Transfer</a></td>
    </tr>
</table>

<br>
<form action="/TestWebJava_war/users">
    <input type="submit" value="Back" />
</body>
</html>