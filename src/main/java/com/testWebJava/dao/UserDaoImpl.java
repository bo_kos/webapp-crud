package com.testWebJava.dao;

import com.testWebJava.entity.User;
import com.testWebJava.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoImpl implements UserDao{

    public final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(User user) {
        String sql ="INSERT INTO public.testweb (name, email, age, money) VALUES (?,?,?,?)";
        jdbcTemplate.update(sql, user.getName(), user.getEmail(), user.getAge(), user.getMoney());
    }

    @Override
    public User getById(int id) {
        String sql = "SELECT * FROM public.testweb WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new UserMapper(), id);
    }

    public List<User> findAll() {
        String sql = "SELECT * FROM public.testweb ORDER BY id";
        return jdbcTemplate.query(sql, new UserMapper());
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM public.testweb WHERE id=?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public void update(User user) {
        String sql = "UPDATE public.testweb SET name=?, email=?, age=?, money=? WHERE id=?";
        jdbcTemplate.update(sql, user.getName(), user.getEmail(), user.getAge(), user.getId(), user.getMoney());
    }

    @Override
    public void transferMoney(int idUser1, double sum, int idUser2) {
        String sql = "UPDATE public.testweb SET money ";
    }
}
