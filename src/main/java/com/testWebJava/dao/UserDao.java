package com.testWebJava.dao;

import com.testWebJava.entity.User;

import java.util.List;

public interface UserDao {

    void save (User user);

    User getById(int id);

    List<User> findAll();

    void delete(int id);

    void update(User user);

    void transferMoney(int idUser1,double sum, int idUser2);

}
